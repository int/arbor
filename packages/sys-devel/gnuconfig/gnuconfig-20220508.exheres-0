# Copyright 2007 Bryan Østergaard <kloeri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

COMMIT_ID=f56a7140386d08a531bcfd444d632b28c61a6329
WORK=${WORKBASE}/config-${COMMIT_ID:0:7}

SUMMARY="GNU config.guess and config.sub scripts"
HOMEPAGE="https://savannah.gnu.org/projects/config"
DOWNLOADS="https://git.savannah.gnu.org/gitweb/?p=config.git;a=snapshot;h=${COMMIT_ID};sf=tgz -> ${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="parts: data documentation"

DEPENDENCIES=""

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/0001-config.sub-recognise-LLVM-canonicalised-windows-trip.patch )

src_prepare() {
    default

    if [[ $(exhost --build) == *-*-*-musl* ]]; then
        # Replace `-gnu` with `-musl` for correct `config.guess` expectations on musl
        edo sed -e '/| Linux |/s/-gnu/-musl/' \
                -i testsuite/config-guess.data

        # Remove incompatible combinations to avoid the `config.sub` test failures like:
        # Invalid configuration `i686-unknown-kopensolaris5.11-musl': Kernel `kopensolaris5.11' not known to work with OS `musl'.
        edo sed -e '/| GNU\/kOpenSolaris |/d' \
                -e '/| GNU\/kFreeBSD |/d' \
                -i testsuite/config-guess.data
    fi
}

src_install() {
    dodir /usr/share/${PN}
    insinto /usr/share/${PN}
    insopts -m0755
    doins config.guess config.sub
    doman doc/config.guess.1 doc/config.sub.1

    expart data /usr/share/gnuconfig
    expart documentation /usr/share/doc /usr/share/man
}


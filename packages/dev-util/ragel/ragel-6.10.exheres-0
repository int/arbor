# Copyright 2009 Mike Kelly <pioto@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# ragel-Don-t-hard-code-gcc.patch
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]

SUMMARY="Ragel State Machine Compiler"
DESCRIPTION="
Ragel compiles executable finite state machines from regular languages.
Ragel targets C, C++, Objective-C, D, Java and Ruby. Ragel state
machines can not only recognize byte sequences as regular expression
machines do, but can also execute code at arbitrary points in the
recognition of a regular language. Code embedding is done using inline
operators that do not disrupt the regular language syntax.
"
HOMEPAGE="https://www.colm.net/open-source/${PN}"
DOWNLOADS="https://www.colm.net/files/${PN}/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
# go
# objc
MYOPTIONS="
    java
    mono
    ruby
"

# go? ( sys-libs/libgo:= )
# objc? ( sys-libs/libobjc:= )
DEPENDENCIES="
    build+run:
        java? ( virtual/jdk:= )
        mono? ( dev-lang/mono )
        ruby? ( dev-lang/ruby:= )
"

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/${PN}-Don-t-hard-code-gcc.patch
)

# needs fig2dev and pdflatex
DEFAULT_SRC_CONFIGURE_PARAMS=( --disable-manual )

# option go || export GOBIN=no
# option objc || export OBJC=no
src_configure() {
    export GOBIN=no
    export OBJC=no
    option java || export JAVAC=no
    option mono || export GMCS=no
    option ruby || export RUBY=no

    default
}


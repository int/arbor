# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'fortune-mod-1.99.1-r2.ebuild' which is
#   Copyright 1999-2008 Gentoo Foundation

require github [ user=shlomif ] cmake

SUMMARY="Get your fortune today"

DOWNLOADS="https://www.shlomifish.org/open-source/projects/${PN}/arcs/${PNV}.tar.xz"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/perl:*
        virtual/pkg-config
    build+run:
        app-text/recode:=
        dev-libs/rinutils
    test:
        dev-perl/File-Find-Object
        dev-perl/IO-All
        dev-perl/Test-Differences
        dev-perl/Test-Trap
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-Don-t-run-tests-involving-valgrind.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DLOCALDIR=/usr/share/fortune
    -DCOOKIEDIR=/usr/share/fortune
)

src_prepare() {
    cmake_src_prepare

    # Remove tests which invoke "cmake && make && make install" and also use
    # games instead of bin, which we sed out below.
    edo rm tests/t/test-fortune-{h-flag,m,o-rot,percent,m--wetpaint}.t

    edo sed \
        -e 's/"games"/"bin"/' \
        -i CMakeLists.txt

    edo sed \
        -e "s|\(share/man/man\)|/usr/\1|" \
        -e "/SET (DATADIR/ s|\${CMAKE_INSTALL_PREFIX}|/usr|" \
        -i cmake/Shlomif_Common.cmake
}

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( INDEX Offensive cookie-files )


# Copyright 2015 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gcc.gnu.org
require toolchain-runtime-libraries

export_exlib_phases src_unpack src_prepare src_configure src_compile src_install

SUMMARY="GCC C Runtime Library"

LICENCES="GPL-2"
SLOT="$(ever major)"

DEPENDENCIES="
    build:
        sys-devel/gcc:${SLOT}
        sys-libs/libstdc++:${SLOT}
"

MYOPTIONS="
    linguas:
        be ca da de el eo es fi fr hr id ja nl ru sr sv tr uk vi zh_CN zh_TW
"

if [[ ${PV} == *_pre* ]] ; then
    ECONF_SOURCE="${WORKBASE}/gcc-$(ever major)-${PV##*_pre}"
    WORK="${WORKBASE}/gcc-$(ever major)-${PV##*_pre}/build"
elif [[ ${PV} == *-rc* ]] ; then
    ECONF_SOURCE="${WORKBASE}/gcc-${PV/rc/RC-}"
    WORK="${WORKBASE}/gcc-${PV/rc/RC-}/build/$(exhost --target)/build"
else
    ECONF_SOURCE="${WORKBASE}/gcc-${PV/_p?(re)/-}"
    WORK="${WORKBASE}/gcc-${PV/_p?(re)/-}/build"
fi

# TODO(compnerd) find a way to expose build tools when cross-compiling and replace
# {AR,RANLIB}_FOR_BUILD accordingly
DEFAULT_SRC_CONFIGURE_PARAMS=(
    AR_FOR_BUILD=$(exhost --build)-ar
    CC_FOR_BUILD=$(exhost --build)-gcc-${SLOT}
    LD_FOR_BUILD=$(exhost --build)-ld
    CPP_FOR_BUILD=$(exhost --build)-gcc-cpp-${SLOT}
    RANLIB_FOR_BUILD=$(exhost --build)-ranlib
    AS_FOR_TARGET=$(exhost --tool-prefix)as
    CC_FOR_TARGET=$(exhost --tool-prefix)gcc-${SLOT}
    LD_FOR_TARGET=$(exhost --tool-prefix)ld
    --disable-bootstrap
    --disable-multilib
    --with-multilib-list=''
    --enable-languages=c
    --disable-fixincludes
    --disable-intl
    --disable-lto
    --disable-libatomic
    --disable-libbacktrace
    --disable-libcpp
    --disable-libssp
    --disable-libquadmath
    --disable-libgomp
    --disable-libvtv
    --disable-vtable-verify
    --with-system-zlib
)

if ever at_least 11.1.0 ; then
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        CXX_FOR_BUILD=$(exhost --build)-g++-${SLOT}
        CXX_FOR_TARGET=$(exhost --tool-prefix)g++-${SLOT}
    )
fi

if ever at_least 10.0.1_pre20200322 ; then
    DEFAULT_SRC_CONFIGURE_PARAMS+=( --enable-tm-clone-registry )
fi

# NOTE(compnerd) libgcc does not have a testsuite
RESTRICT="test"

libgcc_src_unpack() {
    default
    edo mkdir -p "${WORK}"
    edo mkdir -p "${ECONF_SOURCE}/gmp"
    edo chmod +x "${ECONF_SOURCE}/libgcc/configure"
}

libgcc_src_prepare() {
    edo cd "${ECONF_SOURCE}"
    default

    # TODO(compnerd) do this properly via autotools
    edo sed -e '/export_sym_check=/c export_sym_check="'"$(exhost --build)"'-objdump -T"' \
            -i "${ECONF_SOURCE}/gcc/configure"

    if [[ $(exhost --target) == armv7-* ]] ; then
        # arm-cpu.h and arm-isa.h are automatically created for gcc, but not
        # for this split out libgcc. We can use gcc's versions though, which
        # get installed in the .../plugin/include dir.
        if ever at_least 12 ; then
            edo sed \
            -e "/^LIBGCC2_CFLAGS = .*/s/ \$(LIBGCC2_INCLUDES)/& -I\/usr\/$(exhost --build)\/lib\/gcc\/$(exhost --target)\/$(ever range -3)\/plugin\/include/" \
            -e "/^CRTSTUFF_T_CFLAGS/s/ =/& -I\/usr\/$(exhost --build)\/lib\/gcc\/$(exhost --target)\/$(ever range -3)\/plugin\/include/" \
            -i "${ECONF_SOURCE}"/libgcc/Makefile.in
        fi
    fi
}

libgcc_src_configure() {
    CC=$(exhost --tool-prefix)gcc-${SLOT}       \
    CPP=$(exhost --tool-prefix)gcc-cpp-${SLOT}  \
    CXX=$(exhost --tool-prefix)g++-${SLOT}      \
    default
}

libgcc_src_compile() {
    local build=$(exhost --build) ; build="${build//-/_}"
    local build_cflags_var=${build}_CFLAGS
    local build_cppflags_var=${build}_CPPFLAGS
    local build_cxxflags_var=${build}_CXXFLAGS
    local build_ldflags_var=${build}_LDFLAGS
    local LD_FOR_BUILD=$(exhost --build)-ld
    local CC_FOR_BUILD=$(exhost --build)-gcc-${SLOT}
    local CPP_FOR_BUILD=$(exhost --build)-gcc-cpp-${SLOT}
    local CXX_FOR_BUILD=$(exhost --build)-g++-${SLOT}

    emake all-build-libiberty CFLAGS="${!build_cflags_var}" CPPFLAGS="${!build_cppflags_var}" \
        CXXFLAGS="${!build_cxxflags_var}" LDFLAGS="${!build_ldflags_var}" CPP=${CPP_FOR_BUILD}

    emake all-libdecnumber

    edo mkdir -p "${WORK}/gcc"
    edo cd "${WORK}/gcc"
        CC=${CC_FOR_BUILD}                      \
        LD=${LD_FOR_BUILD}                      \
        CPP=${CPP_FOR_BUILD}                    \
        CXX=${CXX_FOR_BUILD}                    \
        CFLAGS="${!build_cflags_var}"           \
        CPPFLAGS="${!build_cppflags_var}"       \
        CXXFLAGS="${!build_cxxflags_var}"       \
        LDFLAGS="${!build_ldflags_var}"         \
        AS_FOR_TARGET=$(exhost --tool-prefix)as \
        CC_FOR_TARGET=$(exhost --tool-prefix)gcc-${SLOT} \
        LD_FOR_TARGET=$(exhost --tool-prefix)ld \
    edo "${ECONF_SOURCE}/gcc/configure"         \
            --host=$(exhost --build)            \
            --build=$(exhost --build)           \
            --target=$(exhost --target)         \
            --prefix=/usr/$(exhost --build)     \
            --libdir=/usr/$(exhost --build)/lib \
            --disable-bootstrap                 \
            --disable-multilib                  \
            --with-multilib-list=''             \
            --with-glibc-version=2.11           \
            --with-sysroot='""'                 \
            --enable-languages=c
    edo sed -e 's,libgcc.mvars:.*$,libgcc.mvars:,' -i Makefile
    if ever at_least 12.0 ; then
        edo make config.h libgcc.mvars tconfig.h tm.h options.h insn-constants.h insn-modes.h version.h
    else
        edo make config.h libgcc.mvars tconfig.h tm.h options.h insn-constants.h insn-modes.h gcov-iov.h
    fi
    edo mkdir -p "${WORK}/gcc/include"

    edo mkdir -p "${WORK}/$(exhost --target)/libgcc"
    edo cd "${WORK}/$(exhost --target)/libgcc"
        CC=$(exhost --tool-prefix)gcc-${SLOT}    \
        AS_FOR_TARGET=$(exhost --tool-prefix)as  \
        CC_FOR_TARGET=$(exhost --tool-prefix)gcc-${SLOT} \
        LD_FOR_TARGET=$(exhost --tool-prefix)ld  \
    edo "${ECONF_SOURCE}/libgcc/configure"       \
            --build=$(exhost --build)            \
            --host=$(exhost --target)            \
            --prefix=/usr/$(exhost --target)     \
            --libdir=/usr/$(exhost --target)/lib \
            --disable-multilib
    emake
}

libgcc_src_install() {
    # NOTE(compnerd) we must manually install here to avoid installing the build time dependencies
    # of gcc and fixheaders from being merged as part of this package.  einstall will by default add
    # 'install' to the make invocation, resulting in host and target builds being installed.
    edo cd "${WORK}/$(exhost --target)/libgcc"
    emake DESTDIR="${IMAGE}" install-shared

    symlink_dynamic_libs ${SLOT} ${PN}_s
    slot_dynamic_libs ${SLOT} ${PN}_s
}


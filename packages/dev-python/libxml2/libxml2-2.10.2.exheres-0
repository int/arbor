# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'libxml2-2.10.0.exheres-0, which is
#    Copyright 2007-2008 Bo Ørsted Andresen
#    Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
#    Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>

require gnome.org [ suffix=tar.xz ]
require setup-py [ import=setuptools work=${PNV}/python ]

SUMMARY="Python bindings for the XML C Parser and Toolkit"
HOMEPAGE="https://gitlab.gnome.org/GNOME/${PN}/-/wikis/home"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/libxml2:2.0[~${PV}]
        sys-libs/zlib[>=1.2.5-r1]
        !dev-libs/libxml2:2.0[<2.10.0-r1] [[
            description = [ dev-python/libxml2 was part of dev-libs/libxml2 previously ]
            resolution = uninstall-blocked-after
        ]]
"

configure_one_multibuild() {
    # Needed to create setup.py, which isn't part of the tarball anymore
    edo pushd ..
    default
    edo popd

    setup-py_configure_one_multibuild
}

test_one_multibuild() {
    edo pushd tests
    for test in $(ls *.py) ; do
        PYTHONPATH="$(ls -d ${PWD}/../build/lib*)" edo ${PYTHON} -B ${test}
    done
    edo popd
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    # Avoid collision with libxml's TODO file
    edo mv "${IMAGE}"/usr/share/doc/${PNVR}/TODO{,-python}
}


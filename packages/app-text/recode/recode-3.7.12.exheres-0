# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'recode-3.6-r2.ebuild' by Gentoo which is:
#   Copyright 1999-2008 Gentoo Foundation

require github [ user=rrthomas release=v${PV} suffix=tar.gz ]
require python [ blacklist=2 multibuild=false ]
require alternatives

SUMMARY="Convert files between various character sets"

LICENCES="LGPL-2.1 || ( GPL-2 GPL-3 )"
SLOT="3"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-apps/help2man
        sys-devel/flex
        sys-devel/gettext
    test:
        dev-python/Cython[python_abis:*(-)?]
    run:
        !app-text/recode:0[<3.7.2-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localedir=/usr/share/locale
    # Don't run tests involving valgrind, they currently fail (3.7)
    --disable-valgrind-tests
    PYTHON="${PYTHON}"
)

src_install() {
    local arch_dependent_alternatives=() other_alternatives=()
    local host=$(exhost --target)

    default

    arch_dependent_alternatives+=(
        /usr/${host}/bin/${PN}         ${PN}-${SLOT}
        /usr/${host}/include/${PN}.h   ${PN}-${SLOT}.h
        /usr/${host}/include/${PN}xt.h ${PN}xt-${SLOT}.h
        /usr/${host}/lib/lib${PN}.a   lib${PN}-${SLOT}.a
        /usr/${host}/lib/lib${PN}.la   lib${PN}-${SLOT}.la
        /usr/${host}/lib/lib${PN}.so   lib${PN}-${SLOT}.so
    )
    other_alternatives+=(
        /usr/share/info/${PN}.info  ${PN}-${SLOT}.info
        /usr/share/man/man1/${PN}.1 ${PN}-${SLOT}.1
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
    alternatives_for _${PN} ${SLOT} ${SLOT} "${other_alternatives[@]}"
}


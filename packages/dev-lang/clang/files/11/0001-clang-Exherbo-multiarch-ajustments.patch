From a26cd15d473c16fd753b615b9e734ceea4d1e7e7 Mon Sep 17 00:00:00 2001
From: Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
Date: Fri, 15 Nov 2019 14:18:30 +0100
Subject: [PATCH 1/4] clang: Exherbo multiarch ajustments

Exherbo multiarch layout being somewhat specific,
some adjustments need to be made wrt the lookup
paths

Signed-off-by: Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
---
 clang/lib/Driver/ToolChains/Gnu.cpp          | 13 +++++
 clang/lib/Driver/ToolChains/Linux.cpp        | 50 +++++++++++++++++++-
 clang/test/Driver/android-ndk-standalone.cpp |  4 +-
 clang/test/Driver/dyld-prefix.c              |  2 +
 clang/test/Driver/linux-ld.c                 |  2 +
 clang/test/Driver/mips-cs.cpp                |  1 +
 clang/test/lit.cfg.py                        |  3 ++
 7 files changed, 73 insertions(+), 2 deletions(-)

diff --git a/clang/lib/Driver/ToolChains/Gnu.cpp b/clang/lib/Driver/ToolChains/Gnu.cpp
index 315988faf40..20b74492535 100644
--- a/clang/lib/Driver/ToolChains/Gnu.cpp
+++ b/clang/lib/Driver/ToolChains/Gnu.cpp
@@ -17,6 +17,7 @@
 #include "Linux.h"
 #include "clang/Config/config.h" // for GCC_INSTALL_PREFIX
 #include "clang/Driver/Compilation.h"
+#include "clang/Driver/Distro.h"
 #include "clang/Driver/Driver.h"
 #include "clang/Driver/DriverDiagnostic.h"
 #include "clang/Driver/Options.h"
@@ -2705,6 +2706,7 @@ void Generic_GCC::AddMultilibPaths(const Driver &D,
     const llvm::Triple &GCCTriple = GCCInstallation.getTriple();
     const std::string &LibPath =
         std::string(GCCInstallation.getParentLibPath());
+    const Distro Distro(D.getVFS(), GCCTriple);
 
     // Add toolchain / multilib specific file paths.
     addMultilibsFilePaths(D, Multilibs, SelectedMultilib,
@@ -2734,6 +2736,17 @@ void Generic_GCC::AddMultilibPaths(const Driver &D,
     //
     // Note that this matches the GCC behavior. See the below comment for where
     // Clang diverges from GCC's behavior.
+    //
+    // On Exherbo, the GCC installation will reside in e.g.
+    //   /usr/x86_64-pc-linux-gnu/lib/gcc/armv7-unknown-linux-gnueabihf/9.2.0
+    // while the matching lib path is
+    //   /usr/armv7-unknown-linux-gnueabihf/lib
+    if (Distro == Distro::Exherbo)
+      addPathIfExists(D,
+                      LibPath + "/../../" + GCCTriple.str() + "/lib/../" +
+                          OSLibDir + SelectedMultilib.osSuffix(),
+                      Paths);
+
     addPathIfExists(D,
                     LibPath + "/../" + GCCTriple.str() + "/lib/../" + OSLibDir +
                         SelectedMultilib.osSuffix(),
diff --git a/clang/lib/Driver/ToolChains/Linux.cpp b/clang/lib/Driver/ToolChains/Linux.cpp
index a272c47e714..1635477e5d7 100644
--- a/clang/lib/Driver/ToolChains/Linux.cpp
+++ b/clang/lib/Driver/ToolChains/Linux.cpp
@@ -732,9 +732,33 @@ void Linux::AddClangSystemIncludeArgs(const ArgList &DriverArgs,
       std::string("/usr/include/") +
       getMultiarchTriple(D, getTriple(), SysRoot);
   const StringRef AndroidMultiarchIncludeDirs[] = {AndroidMultiarchIncludeDir};
-  if (getTriple().isAndroid())
+  const llvm::Triple &Triple = getTriple();
+  if (Triple.isAndroid())
     MultiarchIncludeDirs = AndroidMultiarchIncludeDirs;
 
+  // Exherbo's multiarch layout is /usr/<triple>/include and not
+  // /usr/include/<triple>
+  const Distro Distro(D.getVFS(), Triple);
+  const StringRef ExherboMultiarchIncludeDirs[] = {"/usr/" + Triple.str() +
+                                                   "/include"};
+  const StringRef ExherboI686MuslMultiarchIncludeDirs[] = {
+      "/usr/i686-pc-linux-musl/include"};
+  const StringRef ExherboI686MultiarchIncludeDirs[] = {
+      "/usr/i686-pc-linux-gnu/include"};
+  if (Distro == Distro::Exherbo) {
+    switch (Triple.getArch()) {
+    case llvm::Triple::x86:
+      if (Triple.isMusl())
+        MultiarchIncludeDirs = ExherboI686MuslMultiarchIncludeDirs;
+      else
+        MultiarchIncludeDirs = ExherboI686MultiarchIncludeDirs;
+      break;
+    default:
+      MultiarchIncludeDirs = ExherboMultiarchIncludeDirs;
+      break;
+    }
+  }
+
   for (StringRef Dir : MultiarchIncludeDirs) {
     if (D.getVFS().exists(SysRoot + Dir)) {
       addExternCSystemInclude(DriverArgs, CC1Args, SysRoot + Dir);
@@ -769,8 +793,32 @@ void Linux::addLibStdCxxIncludePaths(const llvm::opt::ArgList &DriverArgs,
 
   StringRef LibDir = GCCInstallation.getParentLibPath();
   StringRef TripleStr = GCCInstallation.getTriple().str();
+  const Driver &D = getDriver();
   const Multilib &Multilib = GCCInstallation.getMultilib();
+  const std::string GCCMultiarchTriple =
+      getMultiarchTriple(D, GCCInstallation.getTriple(), D.SysRoot);
+  const llvm::Triple &Triple = getTriple();
+  const std::string TargetMultiarchTriple =
+      getMultiarchTriple(D, Triple, D.SysRoot);
   const GCCVersion &Version = GCCInstallation.getVersion();
+  const Distro Distro(D.getVFS(), Triple);
+
+  // On Exherbo the consecutive addLibStdCXXIncludePaths call would evaluate to:
+  //   LibDir = /usr/lib/gcc/<triple>/9.2.0/../../..
+  //          = /usr/lib/
+  //   LibDir + "/../include" = /usr/include
+  // addLibStdCXXIncludePaths would then check if "/usr/include/c++/<version>"
+  // exists, and add that as include path when what we want is
+  // "/usr/<triple>/include/c++/<version>"
+  // Note that "/../../" is needed and not just "/../" as /usr/include points to
+  // /usr/host/include
+  if (Distro == Distro::Exherbo)
+    if (addLibStdCXXIncludePaths(LibDir.str() + "/../../" + TripleStr +
+                                     "/include",
+                                 "/c++/" + Version.Text, TripleStr,
+                                 GCCMultiarchTriple, TargetMultiarchTriple,
+                                 Multilib.includeSuffix(), DriverArgs, CC1Args))
+      return;
 
   const std::string LibStdCXXIncludePathCandidates[] = {
       // Android standalone toolchain has C++ headers in yet another place.
diff --git a/clang/test/Driver/android-ndk-standalone.cpp b/clang/test/Driver/android-ndk-standalone.cpp
index c4d93993478..7ce982ddb1c 100644
--- a/clang/test/Driver/android-ndk-standalone.cpp
+++ b/clang/test/Driver/android-ndk-standalone.cpp
@@ -1,6 +1,8 @@
+// XFAIL: exherbo
+
 // Test header and library paths when Clang is used with Android standalone
 // toolchain.
-//
+
 // RUN: %clang -no-canonical-prefixes %s -### -o %t.o 2>&1 \
 // RUN:     -target arm-linux-androideabi21 \
 // RUN:     -B%S/Inputs/basic_android_ndk_tree \
diff --git a/clang/test/Driver/dyld-prefix.c b/clang/test/Driver/dyld-prefix.c
index 5a79874b567..f20824ae18a 100644
--- a/clang/test/Driver/dyld-prefix.c
+++ b/clang/test/Driver/dyld-prefix.c
@@ -1,3 +1,5 @@
+// XFAIL: exherbo
+
 // RUN: touch %t.o
 
 // RUN: %clang -target i386-unknown-linux --dyld-prefix /foo -### %t.o 2>&1 | FileCheck --check-prefix=CHECK-32 %s
diff --git a/clang/test/Driver/linux-ld.c b/clang/test/Driver/linux-ld.c
index ec539522c25..3f46a127762 100644
--- a/clang/test/Driver/linux-ld.c
+++ b/clang/test/Driver/linux-ld.c
@@ -1,3 +1,5 @@
+// XFAIL: exherbo
+
 // General tests that ld invocations on Linux targets sane. Note that we use
 // sysroot to make these tests independent of the host system.
 //
diff --git a/clang/test/Driver/mips-cs.cpp b/clang/test/Driver/mips-cs.cpp
index 6ef4c5d4350..7b2e89919f1 100644
--- a/clang/test/Driver/mips-cs.cpp
+++ b/clang/test/Driver/mips-cs.cpp
@@ -1,4 +1,5 @@
 // REQUIRES: mips-registered-target
+// XFAIL: exherbo
 //
 // Check frontend and linker invocations on Mentor Graphics MIPS toolchain.
 //
diff --git a/clang/test/lit.cfg.py b/clang/test/lit.cfg.py
index 0550ce10d6b..8d17c202f36 100644
--- a/clang/test/lit.cfg.py
+++ b/clang/test/lit.cfg.py
@@ -196,5 +196,8 @@ if macOSSDKVersion is not None:
 if os.path.exists('/etc/gentoo-release'):
     config.available_features.add('gentoo')
 
+if os.path.exists('/etc/exherbo-release'):
+    config.available_features.add('exherbo')
+
 if config.enable_shared:
     config.available_features.add("enable_shared")
-- 
2.26.2


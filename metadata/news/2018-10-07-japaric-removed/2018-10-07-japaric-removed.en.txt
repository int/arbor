Title: repository/japaric has been removed from ::unavailable-unofficial
Author: Alexander Kapshuna
Content-Type: text/plain
Posted: 2018-10-07
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: repository/japaric

::japaric[1] has been removed from ::unavailable-unofficial since it is
not maintained anymore.

If you use any packages from ::japaric, you can resurrect them at any
time by following the instructions on our Contributing[2] doc and
maintain them in your own repository.

[1]: https://github.com/japaric/exheres
[2]: https://exherbo.org/docs/contributing.html

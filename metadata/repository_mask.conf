(
    app-admin/66-exherbo[~scm]
    app-admin/eclectic[~scm]
    app-admin/s6-exherbo[~scm]
    app-arch/libarchive[~scm]
    app-editors/e4r[~scm]
    app-misc/screen[~scm]
    app-shells/bash-completion[~scm]
    app-shells/zsh[~scm]
    app-text/djvu[~scm]
    app-vim/exheres-syntax[~scm]
    dev-db/xapian-core[~scm]
    dev-lang/clang[~scm]
    dev-lang/llvm[~scm]
    dev-libs/compiler-rt[~scm]
    dev-libs/fmt[~scm]
    dev-libs/oblibs[~scm]
    dev-libs/pinktrace[~scm]
    dev-libs/pugixml[~scm]
    dev-scm/git-remote-helpers[~scm]
    dev-util/exherbo-dev-tools[~scm]
    dev-util/ltrace[~scm]
    dev-util/systemtap[~scm]
    dev-util/tig[~scm]
    dev-util/valgrind[~scm]
    media-libs/jbig2dec[~scm]
    net-irc/irssi[~scm]
    net-wireless/iw[~scm]
    net-wireless/wireless-regdb[~scm]
    net-wireless/wpa_supplicant[~scm]
    net-www/elinks[~scm]
    sys-apps/66[~scm]
    sys-apps/66-tools[~scm]
    sys-apps/dbus[~scm]
    sys-apps/eudev[~scm]
#    sys-apps/paludis[~scm]
    sys-apps/multiload[~scm]
    sys-apps/sydbox[~scm]
    sys-apps/systemd[~scm]
    sys-boot/dracut[~scm]
    sys-boot/efibootmgr[~scm]
    sys-devel/lld[~scm]
    sys-devel/lldb[~scm]
    sys-devel/meson[~scm]
    sys-devel/ninja[~scm]
    sys-fs/btrfs-progs[~scm]
    sys-fs/exfatprogs[~scm]
    sys-libs/gcompat[~scm]
    sys-libs/libc++[~scm]
    sys-libs/libc++abi[~scm]
    sys-libs/llvm-libunwind[~scm]
    sys-libs/musl[~scm]
    sys-libs/musl-compat[~scm]
    sys-libs/openmp[~scm]
) [[
    *author = [ Exherbo developers ]
    *token = scm
    *description = [ Mask scm versions ]
]]

dev-util/ccache [[
    author = [ Thomas Anderson <tanderson@caltech.edu> ]
    date = [ 14 Apr 2015 ]
    token = broken
    description = [
        UNSUPPORTED: Results in subtle, often undetectable breakage. Don't use it to compile packages or you get to keep both pieces.
    ]
]]

media-libs/jasper[<2.0.28] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 Mar 2021 ]
    token = security
    description = [ CVE-2021-3443 ]
]]

net-misc/curl[<7.85.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 31 Aug 2022 ]
    token = security
    description = [ CVE-2022-35252 ]
]]

app-admin/sudo[<1.9.5_p2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 26 Jan 2021 ]
    token = security
    description = [ CVE-2021-3156 ]
]]

(
    media-libs/libpng:1.2[<1.2.57]
    media-libs/libpng:1.5[<1.5.28]
    media-libs/libpng:1.6[<1.6.27]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 03 Jan 2017 ]
    *token = security
    *description = [ CVE-2016-10087 ]
]]

app-arch/libzip[<1.3.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Sep 2017 ]
    token = security
    description = [ CVE-2017-12858, CVE-2017-14107 ]
]]

dev-libs/expat[<2.4.5] [[
    author = [ Heiko Becker <tgurr@exherbo.org> ]
    date = [ 19 Feb 2022 ]
    token = security
    description = [ CVE-2022-{25235,25236,25313,25314,25315} ]
]]

media-libs/raptor[<2.0.15-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Nov 2020 ]
    token = security
    description = [ CVE-2017-18926 ]
]]

dev-lang/python:2.7[<2.7.18-r2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 27 Jan 2021 ]
    token = security
    description = [ CVE-2019-20907, CVE-2020-{27619,8492} CVE-2021-3177 ]
]]

(
    dev-lang/python:3.7[<3.7.14]
    dev-lang/python:3.8[<3.8.14]
    dev-lang/python:3.9[<3.9.14]
    dev-lang/python:3.10[<3.10.7]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 07 Sep 2022 ]
    *token = security
    *description = [ CVE-2020-10735 ]
]]

net-libs/libotr[<=3.2.0] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 30 Aug 2012 ]
    token = security
    description = [ CVE-2012-2369 ]
]]

net-print/cups[<2.4.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 May 2022 ]
    token = security
    description = [ CVE-2022-26691 ]
]]

dev-libs/dbus-glib[<0.100.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Mar 2013 ]
    token = security
    description = [ CVE-2013-0292 ]
]]

dev-libs/libxml2[<2.10.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Aug 2022 ]
    token = security
    description = [ CVE-2022-2309 ]
]]

media-libs/tiff[<4.4.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Jun 2022 ]
    token = security
    description = [ CVE-2022-{0561,0562,0865,0891,0907,0908,0909,0924,1210,1354,1355,1622,1623,22844} ]
]]

sys-apps/dbus[<1.12.18] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Jun 2020 ]
    token = security
    description = [ CVE-2020-12049 ]
]]

app-crypt/gnupg[<2.3.7] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 13 Jul 2022 ]
    token = security
    description = [ CVE-2022-34903 ]
]]

(
    dev-libs/icu:57.1[<57.1-r1]
    dev-libs/icu:58.1[<58.2-r1]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 23 May 2017 ]
    *token = security
    *description = [ CVE-2017-7867, CVE-2017-7868 ]
]]

net-misc/openssh[<7.9_p1-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Mar 2019 ]
    token = security
    description = [ CVE-2018-20685, CVE-2019-6109, CVE-2019-6111 ]
]]

dev-utils/ack[<2.12] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 13 Dez 2013 ]
    token = security
    description = [ http://beyondgrep.com/security/ ]
]]

dev-libs/pinktrace[~0-scm] [[
    author = [ Ali Polatel <alip@exherbo.org> ]
    date = [ 09 Jan 2014 ]
    token = scm
    description = [ Mask scm version ]
]]

sys-apps/file[<5.37-r2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 01 Nov 2019 ]
    token = security
    description = [ CVE-2019-18218 ]
]]

dev-libs/gnutls[<3.7.7] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Aug 2022 ]
    token = security
    description = [ CVE-2022-2509 ]
]]

net-print/cups-filters[<1.4.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Dec 2015 ]
    token = security
    description = [ CVE-2015-8560 ]
]]

app-arch/lzo[<2.07] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Jul 2014 ]
    token = security
    description = [ CVE-2014-4607 ]
]]

net-libs/cyrus-sasl[<2.1.28] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 23 Feb 2022 ]
    token = security
    description = [ CVE-2019-19906, CVE-2022-24407 ]
]]

app-crypt/gpgme[<1.5.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 01 Aug 2014 ]
    token = security
    description = [ CVE-2014-3564 ]
]]

dev-libs/libgcrypt[<1.9.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 29 Jan 2021 ]
    token = security
    description = [ https://dev.gnupg.org/T5275, CVE not yet available ]
]]

app-shells/bash[<4.3_p30] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Oct 2014 ]
    token = security
    description = [ CVE-2014-6271, CVE-2014-6277, CVE-2014-6278, CVE-2014-7169, CVE-2014-7186, CVE-2014-7187 ]
]]

net-misc/wget[<1.19.5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 07 May 2018 ]
    token = security
    description = [ CVE-2018-0494 ]
]]

dev-libs/libksba[<1.3.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 Nov 2014 ]
    token = security
    description = [ CVE-2014-9087 ]
]]

net-dns/bind[<9.16.28] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 Apr 2022 ]
    token = security
    description = [ CVE-2021-25220, CVE-2022-0396 ]
]]

net/ntp[<4.2.8_p13] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 11 Mar 2019 ]
    token = security
    description = [ CVE-2019-8936 ]
]]

dev-scm/subversion[<1.10.7] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 11 Feb 2021 ]
    token = security
    description = [ CVE-2020-17525 ]
]]

(
    dev-libs/libevent:0[<2.0.22-r3]
    dev-libs/libevent:2.1[<2.1.6]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 23 May 2017 ]
    *token = security
    *description = [ CVE-2016-1019{5,6,7} ]
]]

sys-devel/patch[<2.7.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Feb 2015 ]
    token = security
    description = [ CVE-2015-1196 ]
]]

sys-apps/grep[<2.22] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Nov 2015 ]
    token = security
    description = [ CVE-2015-1345 ]
]]

sys-libs/glibc[<2.34-r25] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 31 Jan 2022 ]
    token = security
    description = [ CVE-2021-3998 ]
]]

sys-fs/e2fsprogs[<1.45.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Sep 2019 ]
    token = security
    description = [ CVE-2019-5094 ]
]]

(
    dev-lang/perl:5.28[<5.28.3]
    dev-lang/perl:5.30[<5.30.3]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 02 Jun 2020 ]
    *token = security
    *description = [ CVE-2020-{10543,10878,12723} ]
]]

media-libs/gd[<2.3.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Sep 2020 ]
    token = security
    description = [ CVE-2018-{5711,14553,1000222}, CVE-2019-697{7,8}, CVE-2019-11038 ]
]]

app-arch/libtar[<1.2.20] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Mar 2015 ]
    token = security
    description = [ CVE-2013-4397 ]
]]

net-libs/libssh2[<1.8.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 19 Mar 2019 ]
    token = security
    description = [ CVE-2019-{3855,3856,3857,3858,3859,3860,3861,3862,3863} ]
]]

dev-libs/libtasn1[<4.12-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Sep 2017 ]
    token = security
    description = [ CVE-2017-10790 ]
]]

sys-apps/paludis[~2.4.0] [[
    author = [ Thomas Anderson <tanderson@caltech.edu> ]
    date = [ 15 Apr 2015 ]
    description = [ Doesn't support multiarch, downgrading from scm *will* break your system ]
    token = broken
]]

dev-libs/pcre[<8.40-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Jun 2017 ]
    token = security
    description = [ CVE-2017-6004 ]
]]

dev-db/sqlite:3[<3.39.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 25 Jul 2022 ]
    token = security
    description = [ CVE-2022-35737 ]
]]

app-antivirus/clamav[<0.104.2] [[
    author = [ Tom Briden <tom@decompile.me.uk> ]
    date = [ 14 Feb 2022 ]
    token = security
    description = [ CVE-2022-20698 ]
]]

dev-libs/xerces-c[<3.1.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 May 2015 ]
    token = security
    description = [ CVE-2015-0252 ]
]]

(
    sys-fs/fuse:0[<2.9.8]
    sys-fs/fuse:3[<3.2.5]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 25 Jul 2018 ]
    *token = security
    *description = [ CVE-2018-10906 ]
]]

sys-libs/pam[<1.5.0] [[
    author = [ Tom Briden <tom@decompile.me.uk> ]
    date = [ 01 Dec 2020 ]
    token = security
    description = [ CVE-2020-27780 ]
]]

sys-apps/less[<475] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Jul 2015 ]
    token = security
    description = [ CVE-2014-9488 ]
]]

net-dns/libidn[<1.33-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Sep 2017 ]
    token = security
    description = [ CVE-2017-14062 ]
]]

sys-fs/xfsprogs[<3.2.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Aug 2015 ]
    token = security
    description = [ CVE-2012-2150 ]
]]

net-nds/rpcbind[<0.2.4-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2017-8779 ]
]]

sys-apps/xinetd[<2.3.15-r3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Nov 2015 ]
    token = security
    description = [ CVE-2013-4342 ]
]]

app-arch/unzip[<6.0-r7] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 28 Aug 2022 ]
    token = security
    description = [ CVE-2022-0529, CVE-2022-0530 ]
]]

(
    sys-apps/systemd[<249.8]
    sys-apps/systemd[>=250.0&<250.2]
) [[
    *author = [ Heiko <heirecka@exherbo.org> ]
    *date = [ 12 Jan ]
    *token = security
    *description = [ CVE-2021-3997 ]
]]

app-arch/p7zip[<16.02-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Feb 2018 ]
    token = security
    description = [ CVE-2017-17969, CVE-2018-5996 ]
]]

sys-boot/grub[<2.06-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Jan 2022 ]
    token = security
    description = [ CVE-2021-3981 ]
]]

dev-libs/libxslt[<1.1.35] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 18 Feb 2022 ]
    token = security
    description = [ CVE-2021-30560 ]
]]

app-misc/screen[<4.9.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 25 Feb 2022 ]
    token = security
    description = [ CVE-2021-26937 ]
]]

dev-libs/botan[<1.10.17] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 04 Oct 2017 ]
    token = security
    description = [ CVE-2016-14737 ]
]]

(
    dev-scm/git[<2.36.2]
    dev-scm/git[>=2.37.0&<2.37.1]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 12 Apr 2022 ]
    *token = security
    *description = [ CVE-2022-29187 ]
]]

sys-apps/busybox[<1.34.1-r1] [[
    author = [ Lucas Roberto <sigmw@protonmail.com> ]
    date = [ 26 Aug 2022 ]
    token = security
    description = [ CVE-2022-28391 ]
]]

media-gfx/ImageMagick[<6.9.11.57] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Jan 2021 ]
    token = security
    description = [ CVE-2020-29599 ]
]]

dev-libs/jansson[<2.7-r1] [[
    author = [ Kylie McClain <somasis@exherbo.org> ]
    date = [ 05 May 2016 ]
    token = security
    description = [ CVE-2016-4425 ]
]]

net-wireless/wpa_supplicant[<2.8] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 31 Jul 2019 ]
    token = security
    description = [ CVE-2019-949{4,5,9} ]
]]

net-misc/openntpd[<6.0_p1] [[
    author = [ Kylie McClain <somasis@exherbo.org> ]
    date = [ 31 May 2016 ]
    token = security
    description = [ CVE-2016-5117 ]
]]

media-gfx/GraphicsMagick[<1.3.36] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Jan 2021 ]
    token = security
    description = [ CVE-2020-12672 ]
]]

app-arch/libarchive[<3.6.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Feb 2022 ]
    token = security
    description = [ CVE-2021-31566, CVE-2021-36976 ]
]]

(
    dev-lang/node[<14.20.0]
    dev-lang/node[>=16&<16.16.0]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 11 Jul 2022 ]
    *token = security
    *description = [ CVE-2022-2097, CVE-2022-3221{2,3,4,5}, CVE-2022-32223 ]
]]

app-arch/p7zip[<15.14.1-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Jun 2016 ]
    token = security
    description = [ CVE-2016-2334, CVE-2016-2335 ]
]]

sys-devel/flex[<2.6.1-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 26 Aug 2016 ]
    token = security
    description = [ CVE-2016-6354 ]
]]

app-arch/tar[<1.31] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jan 2018 ]
    token = security
    description = [ CVE-2018-20482 ]
]]

dev-lang/guile:1.8[<1.8.8-r3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 22 Dec 2016 ]
    token = security
    description = [ CVE-2016-8605 ]
]]

dev-lang/guile:2[<2.0.13] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 17 Nov 2016 ]
    token = security
    description = [ CVE-2016-8605 CVE-2016-8606 ]
]]

net-dialup/ppp[<2.4.7-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Nov 2016 ]
    token = security
    description = [ CVE-2015-3310 ]
]]

sys-libs/cracklib[<2.9.6-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 08 Dec 2016 ]
    token = security
    description = [ CVE-2015-6318 ]
]]

sys-libs/musl[<1.1.16] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Jan 2017 ]
    token = security
    description = [ CVE-2016-8859 ]
]]

media-libs/jbig2dec[<0.13-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 31 May 2017 ]
    token = security
    description = [ CVE-2017-{7885,7975,7976,9216} ]
]]

app-text/ghostscript[<9.54.0-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Sep 2021 ]
    token = security
    description = [ CVE-2021-3781 ]
]]

sys-fs/ntfs-3g_ntfsprogs[<2022.5.17] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 26 May 2022 ]
    token = security
    description = [ CVE-2021-46790, CVE-2022-{30783,30784,30785,30786,30787,
                                              30788,30789} ]
]]

app-editors/vim[<9.0.0090] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Jul 2022 ]
    token = security
    description = [ CVE-2022-2125, CVE-2022-2126, CVE-2022-2129 ]
]]

sys-apps/shadow[<4.12.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Aug 2022 ]
    token = security
    description = [ CVE-2013-4235 ]
]]

net-dns/c-ares[<1.17.2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 10 Aug 2021 ]
    token = security
    description = [ CVE-2021-3672 ]
]]

(
    dev-libs/libressl[<3.1.5]
    dev-libs/libressl[>=3.2&<3.2.6]
    dev-libs/libressl[>=3.3&<3.3.4]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 26 Aug 2021 ]
    *token = security
    *description = [ CVE-2021-3712 ]
]]

net-libs/libtirpc[<1.3.2-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Aug 2022 ]
    token = security
    description = [ CVE-2021-46828 ]
]]

dev-lang/go[<1.18.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Aug 2022 ]
    token = security
    description = [ CVE-2022-32189 ]
]]

dev-libs/libbsd[<0.8.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Feb 2016 ]
    token = security
    description = [ CVE-2016-2090 ]
]]

net-directory/openldap[<2.6.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2022 ]
    token = security
    description = [ CVE-2022-29155 ]
]]

sys-devel/binutils[<2.28-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Jun 2017 ]
    token = security
    description = [ CVE-2017-6969, CVE-2017-6966, CVE-2017-6965, CVE-2017-9041, CVE-2017-9040,
                    CVE-2017-9042, CVE-2017-9039, CVE-2017-9038, CVE-2017-8421, CVE-2017-8396,
                    CVE-2017-8397, CVE-2017-8395, CVE-2017-8394, CVE-2017-8393, CVE-2017-8398,
                    CVE-2017-7614 ]
]]

app-arch/unrar[<5.5.5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 23 Jun 2017 ]
    token = security
    description = [ CVE-2012-6706 ]
]]

dev-scm/mercurial[<4.3.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Aug 2017 ]
    token = security
    description = [ CVE-2017-1000115, CVE-2017-1000116 ]
]]

dev-scm/subversion[<1.14.2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 12 Apr 2022 ]
    token = security
    description = [ CVE-2021-28544 ]
]]

sys-apps/coreutils[<8.28] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 2 Sep 2017 ]
    token = security
    description = [ CVE-2017-7476 ]
]]

app-text/podofo[<0.9.5_p20170903] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 21 Sep 2017 ]
    token = security
    description = [ CVE-2017-{5852,5853,5854,5855,5886,6840,6844,6847,7378,
                              7379,7380,7994,8787} ]
]]

net-dns/dnsmasq[<2.85] [[
    author = [ Tom Briden <tom@decompile.me.uk> ]
    date = [ 27 Aug 2021 ]
    token = security
    description = [ CVE-2021-3448 ]
]]

mail-mta/exim[<4.94.2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 04 May 2021 ]
    token = security
    description = [ CVE-2020-{28007,28008,28009,28010,28011,28012,28013,28014,
			28015,28016,28017,28018,28019,28020,28021,28022,28023,
			28024,28025,28026} ]
]]

net-misc/rsync[<3.2.4-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 04 Aug 2022 ]
    token = security
    description = [ CVE-2022-29154 ]
]]

sys-libs/ncurses[<6.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Feb 2018 ]
    token = security
    description = [ CVE-2017-{10684,10685,11112,11113,13728,13729,13730,
                              13731,13732,13733,13734,16879} ]
]]

app-arch/gcab[<1.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 01 Feb 2018 ]
    token = security
    description = [ CVE-2018-5345 ]
]]

net-irc/irssi[<1.2.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 Oct 2020 ]
    token = security
    description = [ CVE-2019-15717 ]
]]

net-dns/idnkit[>=2.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Mar 2018 ]
    token = broken
    description = [ Breaks its solely users bind{,-tools} ]
]]

(
    dev-db/postgresql[>=10&<10.22]
    dev-db/postgresql[>=11&<11.17]
    dev-db/postgresql[>=12&<12.12]
    dev-db/postgresql[>=13&<13.8]
    dev-db/postgresql[>=14&<14.5]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 15 Aug 2022 ]
    *token = security
    *description = [ CVE-2022-2625 ]
]]

net-misc/dhcp[<4.3.6_p1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Mar 2018 ]
    token = security
    description = [ CVE-2018-573{2,3} ]
]]

sys-apps/util-linux[<2.37.4] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 14 Feb 2022 ]
    token = security
    description = [ CVE-2022-0563 ]
]]

(
    dev-lang/php:7.4[<7.4.30]
    dev-lang/php:8.0[<8.0.20]
    dev-lang/php:8.1[<8.1.7]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 13 Jun 2022 ]
    *token = security
    *description = [ CVE-2022-31625, CVE-2022-31626 ]
]]

sys-apps/busybox[<1.28.4-r1] [[
    *author = [ Kylie McClain <somasis@exherbo.org> ]
    *date = [ 7 Jun 2018 ]
    *token = security
    *description = [ Using busybox wget is insecure for https urls, as there is
                     no certificate validation done. Busybox >=1.28.4-r1 changes
                     configuration options to disable HTTPS functionality and
                     error when used. ]
]]

sys-process/procps[<3.3.15] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 03 Jun 2018 ]
    token = security
    description = [ CVE-2018-{1122,1123,1124,1125,1126} ]
]]

dev-libs/crossguid[>=0.2.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Jul 2018 ]
    token = testing
    description = [ Breaks its solely user Kodi ]
]]

app-arch/sharutils[<4.15.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Aug 2018 ]
    token = security
    description = [ CVE-2018-1000097 ]
]]

app-arch/cabextract[<1.8] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Nov 2018 ]
    token = security
    description = [ CVE-2018-18584 ]
]]

dev-util/elfutils[<0.176] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 18 Feb 2019 ]
    token = security
    description = [  CVE-2019-{7146,7148,7149,7150,7664,7665} ]
]]

sys-devel/gettext[<0.19.8.1-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Jan 2019 ]
    token = security
    description = [ CVE-2018-18751 ]
]]

net-libs/ldns[<1.7.0-rc1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 05 Feb 2019 ]
    token = security
    description = [ CVE-2017-1000231, CVE-2017-1000232 ]
]]

(
    dev-lang/php:5.6
    dev-lang/php:7.0
    dev-lang/php:7.1
    dev-lang/php:7.2
    dev-lang/php:7.3
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 25 Aug 2020 ]
    *token = pending-removal
    *description = [ EOL ]
]]

sys-libs/libseccomp[<2.4.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 15 Mar 2019 ]
    token = security
    description = [ https://www.openwall.com/lists/oss-security/2019/03/15/1 ]
]]

sys-process/cronie[<1.5.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 Apr 2019 ]
    token = security
    description = [ CVE-2019-9704, CVE-2019-9705 ]
]]

dev-libs/zziplib[<0.13.69-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Jun 2019 ]
    token = security
    description = [ CVE-2018-16548, CVE-2018-17828 ]
]]

dev-libs/glib:2[<2.66.8] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 Mar 2021 ]
    token = security
    description = [ CVE-2021-28153 ]
]]

sys-libs/musl[<1.1.23-r1] [[
    author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    date = [ 10 Aug 2019 ]
    token = security
    description = [ CVE-2019-14697 ]
]]

media-gfx/graphviz[<2.42.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Aug 2019 ]
    token = security
    description = [ CVE-2018-10196 ]
]]

(
    dev-lang/ruby:2.7[<2.7.6]
    dev-lang/ruby:3.0[<3.0.4]
    dev-lang/ruby:3.1[<3.1.2]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 10 May 2022 ]
    *token = security
    *description = [ CVE-2022-28738, CVE-2022-28739 ]
]]

dev-libs/libpcap[<1.9.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 06 Oct 2019 ]
    token = security
    description = [ CVE-2018-16301, CVE-2019-{15161,15162,15163,15164,15165} ]
]]

net-dns/libidn2[<2.2.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 31 Oct 2019 ]
    token = security
    description = [ CVE-2019-12290 ]
]]

app-arch/cpio[<2.13] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 06 Nov 2019 ]
    token = security
    description = [ CVE-2015-1197, CVE-2016-2037, CVE-2019-14866 ]
]]

dev-libs/fribidi[<1.0.7-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 09 Nov 2019 ]
    token = security
    description = [ CVE-2019-18397 ]
]]

dev-libs/oniguruma[<6.9.5_p1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Oct 2020 ]
    token = security
    description = [ CVE-2020-26159 ]
]]

app-shells/zsh[<5.8.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 12 Feb 2022 ]
    token = security
    description = [ CVE-2021-45444 ]
]]

dev-libs/glib-networking[<2.64.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Jul 2020 ]
    token = security
    description = [ CVE-2020-13645 ]
]]

media-libs/libjpeg-turbo[<2.0.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 17 Jul 2020 ]
    token = security
    description = [ CVE-2020-13790 ]
]]

sys-fs/cryptsetup[<2.4.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 14 Jan 2022 ]
    token = security
    description = [ CVE-2021-4122 ]
]]

(
    virtual/jdk:15
    virtual/jre:15
    virtual/jdk:16
    virtual/jre:16
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 21 Sep 2020 ]
    *token = testing
    *description = [ Mask non-LTS releases, most software is only tested on LTS ]
]]

mail-filter/procmail[<3.22-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Sep 2020 ]
    token = security
    description = [ CVE-2014-3618, CVE-2017-16844 ]
]]

media-libs/jpeg[<9d] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Oct 2020 ]
    token = security
    description = [ CVE-2020-14152, CVE-2020-14153 ]
]]

dev-scm/gitolite[<3.6.12] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Oct 2020 ]
    token = security
    description = [ CVE-2018-20683 ]
]]

sys-apps/sysstat[<12.4.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 Oct 2020 ]
    token = security
    description = [ CVE-2019-19725 ]
]]

mail-client/mutt[<1.14.7] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 Oct 2020 ]
    token = security
    description = [ CVE-2020-14954 ]
]]

sys-libs/musl[<1.1.24-r1] [[
    author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    date = [ 22 Nov 2020 ]
    token = security
    description = [ CVE-2020-28928 ]
]]

sys-libs/musl[>=1.2.0&<1.2.1-r1] [[
    author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    date = [ 22 Nov 2020 ]
    token = security
    description = [ CVE-2020-28928 ]
]]

dev-libs/openssl[<1.1.1q] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 05 Jul 2022 ]
    token = security
    description = [ CVE-2022-2097 ]
]]

dev-libs/openssl[>=3.0.0&<3.0.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Jul 2022 ]
    token = security
    description = [ CVE-2022-2097, CVE-2022-2274 ]
]]

dev-libs/openssl[>2] [[
    author = [ Marc-Antoine Perennou <keruspe@exherbo.org> ]
    date = [ 05 Dev 2020 ]
    token = experimental
    description = [ Requires extensive testing ]
]]

dev-libs/p11-kit[<0.23.22] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 14 Jan 2021 ]
    token = security
    description = [ CVE-2020-29361, CVE-2020-29362, CVE-2020-29363 ]
]]

sys-apps/gptfdisk[<1.0.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Jan 2021 ]
    token = security
    description = [ CVE-2020-0256, CVE-2021-0308 ]
]]

dev-libs/crypto++[<8.6.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 26 Sep 2021 ]
    token = security
    description = [ CVE-2021-40530 ]
]]

sys-apps/sydbox[>=2.0.0] [[
    author = [ Alï Polatel <alip@exherbo.org> ]
    date = [ 14 Jun 2021 ]
    token = testing
    description = [ Paludis does not support Sydbox API 2 yet ]
]]

dev-libs/libuv[<1.41.0-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Jul 2021 ]
    token = security
    description = [ CVE-2021-22918 ]
]]

net-www/lynx[<2.9.0_pre9] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Aug 2021 ]
    token = security
    description = [ CVE-2021-38165 ]
]]

dev-libs/boost[>=1.80.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 25 Aug 2022 ]
    token = testing
    description = [ New versions are likely to break dependents ]
]]

app-spell/aspell[<0.60.8-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 28 Aug 2021 ]
    token = security
    description = [ CVE-2019-25051 ]
]]

app-arch/lz4[<1.9.3-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 29 Sep 2021 ]
    token = security
    description = [ CVE-2021-3520 ]
]]

dev-libs/yaml-cpp:0.7 [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Oct 2021 ]
    token = testing
    description = [ Breaks some packages. CMake issues https://github.com/jbeder/yaml-cpp/issues/774 ]
]]

dev-libs/libressl[>=3.5] [[
    author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    date = [ 3 May 2022 ]
    token = testing
    description = [ As usual, it currently breaks at least rust ]
]]

dev-libs/libressl[<3.3] [[
    author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    date = [ 28 Nov 2021 ]
    token = security
    description = [ EOL branch (3.2 since 2021-10-18), no security updates ]
]]

dev-lang/tcl[<8.6.12] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Dec 2021 ]
    token = security
    description = [ CVE-2021-35331 ]
]]

app-arch/unshield[<1.5.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 Jan 2022 ]
    token = security
    description = [ CVE-2015-1386 ]
]]

dev-libs/mbedtls[<2.28.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Feb 2022 ]
    token = security
    description = [ CVE-2021-44732 ]
]]

dev-util/ragel[>=7] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 03 Mar 2022 ]
    token = testing
    description = [ Development version ]
]]

sys-libs/zlib[<1.2.12-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Aug 2022 ]
    token = security
    description = [ CVE-2022-37434 ]
]]

app-arch/gzip[<1.12] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 08 Apr 2022 ]
    token = security
    description = [ ZDI-CAN-16587 ]
]]

app-arch/xz[<5.2.5-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 08 Apr 2022 ]
    token = security
    description = [ ZDI-CAN-16587 ]
]]
